'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.logging = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

exports.createPackageFromFile = createPackageFromFile;
exports.createPackage = createPackage;

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _os = require('os');

var _os2 = _interopRequireDefault(_os);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _child_process = require('child_process');

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _handlebars = require('handlebars');

var _handlebars2 = _interopRequireDefault(_handlebars);

var _logging = require('./logging');

var _logging2 = _interopRequireDefault(_logging);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const nugetPath = process.env['NUGET_PATH'] || _path2.default.join(__dirname, '..', 'vendor', 'nuget.exe');

const fsAsync = _bluebird2.default.promisifyAll(_fs2.default);
const tempDir = _os2.default.tmpdir();

const tmpl = fsAsync.readFileSync(_path2.default.join(__dirname, '..', 'templates', 'tmpl.nuspec.hbs'), 'utf-8');
const template = _handlebars2.default.compile(tmpl);

const spawnNuget = async (nuspec, outDir) => {
  return new _bluebird.Promise((resolve, reject) => {
    if (_os2.default.platform() === 'win32') {
      const nuget = (0, _child_process.spawn)(nugetPath, ['pack', nuspec, '-OutputDirectory', outDir]);
      nuget.stderr.on('data', data => {
        _logging2.default.error(`Nuget error: ${data}`);
      });
      nuget.on('close', code => {
        if (code === 0) {
          resolve(code);
        } else {
          reject(`NuGet process returned: ${code}`);
        }
      });
    } else {
      const monoPath = process.env['MONO_PATH'];
      if (!monoPath) return reject('Environment variable "MONO_PATH" notset');
      const nuget = (0, _child_process.spawn)(monoPath, [nugetPath, 'pack', nuspec, '-OutputDirectory', outDir]);
      nuget.on('close', code => {
        if (code === 0) {
          resolve(code);
        } else {
          reject(new Error(`Nuget error: ${code}`));
        }
      });
    }
  });
};

async function createPackageFromFile(nuspec, outDir) {
  return spawnNuget(nuspec, outDir);
}

async function createPackage(manifestInfo, outDir) {
  if (!manifestInfo) {
    return _bluebird.Promise.reject(new Error('Manifest cannot be null or undefined!'));
  }

  if (!outDir) {
    return _bluebird.Promise.reject(new Error('Output directory cannot be null!'));
  }

  const manifest = _extends({}, manifestInfo, {
    acceptLicense: 'false',
    owners: manifestInfo.authors,
    description: '__SUMMARY__',
    releaseNotes: '__SUMMARY__',
    copyright: 'Copyright 2018',
    licenseUrl: 'http://__TEMP__',
    projectUrl: 'http://__TEMP__',
    iconUrl: 'http://__TEMP__',
    tags: manifestInfo.id
  });
  try {
    const { id, version } = manifestInfo;
    const specFile = _path2.default.join(tempDir, `${id}.${version}.nuspec`);
    const spec = template(manifest);
    await fsAsync.writeFileAsync(specFile, spec, 'utf-8');
    _logging2.default.info(`Creating package in directory: ${outDir}`);
    return _bluebird.Promise.resolve((await createPackageFromFile(specFile, outDir)));
  } catch (err) {
    return _bluebird.Promise.reject(err);
  }
}

const logging = exports.logging = _logging2.default;

exports.default = {
  createPackage,
  createPackageFromFile,
  logger: _logging2.default
};
//# sourceMappingURL=index.js.map