'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.error = exports.info = undefined;

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

var _winston = require('winston');

var _winston2 = _interopRequireDefault(_winston);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const info = exports.info = (...args) => _winston2.default.info(_chalk2.default.blue(...args));
const error = exports.error = (...args) => _winston2.default.error(_chalk2.default.bgRed.black(...args));

exports.default = {
  info,
  error
};
//# sourceMappingURL=logging.js.map