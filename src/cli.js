#!/usr/bin/env node
import yargs from 'yargs';

import { logging } from '../lib';

const argv = yargs
  .command('pack', 'pack a given nuspec manifest into a nupkg artifact')
  .example('$0 pack -f sample.nuspec -o my-pkg.nupkg')
  .alias('f', 'file')
  .alias('o', 'outFile')
  .nargs('f', 1)
  .nargs('o', 1)
  .describe('f', 'Path to nuspec file')
  .describe('o', 'Output file name')
  .demandOption(['f'])
  .command('init', 'Initialize a nuspec file with placeholder values')
  .option('exe', {
    default: false,
    describe: 'Use the bundled nuget.exe file to perform the commands',
  })
  .argv;

if (argv.exe) {
  logging.info('Using bundled `nuget.exe` for packaging');
}

logging.info(`File: ${argv.file}`);
