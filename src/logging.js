import chalk from 'chalk';
import winston from 'winston';

export const info = (...args) => winston.info(chalk.blue(...args));
export const error = (...args) => winston.error(chalk.bgRed.black(...args));

export default {
  info,
  error,
};
