import fs from 'fs';
import os from 'os';
import path from 'path';
import { spawn } from 'child_process';

import bluebird, { Promise } from 'bluebird';
import handlebars from 'handlebars';

import logger from './logging';

const nugetPath = process.env['NUGET_PATH'] || path.join(__dirname, '..', 'vendor', 'nuget.exe');

const fsAsync = bluebird.promisifyAll(fs);
const tempDir = os.tmpdir();

const tmpl = fsAsync.readFileSync(path.join(__dirname, '..', 'templates', 'tmpl.nuspec.hbs'), 'utf-8');
const template = handlebars.compile(tmpl);

const spawnNuget = async (nuspec, outDir) => {
  return new Promise((resolve, reject) => {
    if (os.platform() === 'win32') {
      const nuget = spawn(nugetPath, ['pack', nuspec, '-OutputDirectory', outDir]);
      nuget.stderr.on('data', (data) => {
        logger.error(`Nuget error: ${data}`);
      });
      nuget.on('close', (code) => {
        if (code === 0) {
          resolve(code);
        } else {
          reject(`NuGet process returned: ${code}`);
        }
      });
    } else {
      const monoPath = process.env['MONO_PATH'];
      if (!monoPath) return reject('Environment variable "MONO_PATH" notset');
      const nuget = spawn(monoPath, [nugetPath, 'pack', nuspec, '-OutputDirectory', outDir]);
      nuget.on('close', (code) => {
        if (code === 0) {
          resolve(code);
        } else {
          reject(new Error(`Nuget error: ${code}`));
        }
      });
    }
  });
};

export async function createPackageFromFile(nuspec, outDir) {
  return spawnNuget(nuspec, outDir);
}

export async function createPackage(manifestInfo, outDir) {
  if (!manifestInfo) {
    return Promise.reject(new Error('Manifest cannot be null or undefined!'));
  }

  if (!outDir) {
    return Promise.reject(new Error('Output directory cannot be null!'));
  }

  const manifest = {
    ...manifestInfo,
    acceptLicense: 'false',
    owners: manifestInfo.authors,
    description: '__SUMMARY__',
    releaseNotes: '__SUMMARY__',
    copyright: 'Copyright 2018',
    licenseUrl: 'http://__TEMP__',
    projectUrl: 'http://__TEMP__',
    iconUrl: 'http://__TEMP__',
    tags: manifestInfo.id,
  };
  try {
    const { id, version } = manifestInfo;
    const specFile = path.join(tempDir, `${id}.${version}.nuspec`);
    const spec = template(manifest);
    await fsAsync.writeFileAsync(specFile, spec, 'utf-8');
    logger.info(`Creating package in directory: ${outDir}`);
    return Promise.resolve(await createPackageFromFile(specFile, outDir));
  } catch (err) {
    return Promise.reject(err);
  }
}

export const logging = logger;

export default {
  createPackage,
  createPackageFromFile,
  logger,
};
