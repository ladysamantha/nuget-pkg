#!/usr/bin/env node
'use strict';

var _yargs = require('yargs');

var _yargs2 = _interopRequireDefault(_yargs);

var _lib = require('../lib');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const argv = _yargs2.default.command('pack', 'pack a given nuspec manifest into a nupkg artifact').example('$0 pack -f sample.nuspec -o my-pkg.nupkg').alias('f', 'file').alias('o', 'outFile').nargs('f', 1).nargs('o', 1).describe('f', 'Path to nuspec file').describe('o', 'Output file name').demandOption(['f']).command('init', 'Initialize a nuspec file with placeholder values').option('exe', {
  default: false,
  describe: 'Use the bundled nuget.exe file to perform the commands'
}).argv;

if (argv.exe) {
  _lib.logging.info('Using bundled `nuget.exe` for packaging');
}

_lib.logging.info(`File: ${argv.file}`);

//# sourceMappingURL=nuget-pkg.js.map