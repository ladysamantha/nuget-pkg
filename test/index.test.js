import path from 'path';

import { createPackage } from '../lib/index';

it('will create a package given a (partial) manifest', async () => {
  const manifest = {
    id: 'test-pkg',
    version: '0.0.1',
    description: '0.0.1',
    authors: 'Samantha Enders',
    files: [
      {
        src: path.join(__dirname, 'test.dll'),
        target: 'lib\\net45',
      }
    ]
  };
  return expect(createPackage(manifest, __dirname)).resolves.toBe(0);
});
